import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders } from '@angular/common/http';

export interface Response {
	status: string;
	message: string;
	[index: number]: any;
}

@Injectable({
 providedIn: 'root',
})

export class ApiService {
  
  private headerDict = {
	  'Content-Type': 'application/json',
	  'Accept': 'application/json',
	  'Access-Control-Allow-Headers': 'Content-Type',
	}
  private requestOptions = {                                                                                                                                                                                 
	  headers: new HttpHeaders(this.headerDict), 
	}

  private apiUrl = 'http://reserve-api.fishinfire.com/api.php';

  constructor(private _http: HttpClient) { }

  getTables() {
  	return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=get_tables'),this.requestOptions);
  }
  getTable(id) {
  	return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=get_table&id='+id),this.requestOptions);
  }
  addTable(tableName,tableMaxGuests,tableNumber,tableAdminComments,coordinateX,coordinateY,visualType) {
  	return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=add_table&table_name='+tableName+'&table_max_guests='+tableMaxGuests+'&table_number='+tableNumber+'&table_admin_comments='+tableAdminComments+'&coordinate_x='+coordinateX+'&coordinate_y='+coordinateY+'&visual_type='+visualType),this.requestOptions);
  }
  removeTable(id) {
  	return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=remove_table&id='+id),this.requestOptions);
  }
  updateTable(tableId,tableName,tableMaxGuests,tableNumber,tableAdminComments,coordinateX,coordinateY,visualType) {
  	return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=update_table&table_name='+tableName+'&table_max_guests='+tableMaxGuests+'&table_number='+tableNumber+'&table_admin_comments='+tableAdminComments+'&id='+tableId+'&coordinate_x='+coordinateX+'&coordinate_y='+coordinateY+'&visual_type='+visualType),this.requestOptions);
  }
  getSchedule(dateTimeStart,dateTimeEnd) {
    return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=get_schedule'+'&datetime_start='+dateTimeStart+'&datetime_end='+dateTimeEnd),this.requestOptions);
  }
  getScheduleRow(scheduleId) {
  	return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=get_schedule_row&id='+scheduleId),this.requestOptions);
  }
  addToSchedule(tableId,dateTimeStart,dateTimeEnd,countGuests,reserveComments,guestName,guestPhone) {
    return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=add_to_schedule&table_id='+tableId+'&datetime_start='+dateTimeStart+'&datetime_end='+dateTimeEnd+'&count_guests='+countGuests+'&comments='+reserveComments+'&guest_name='+guestName+'&guest_phone='+guestPhone),this.requestOptions);
  }
  removeFromSchedule(scheduleId) {
  	return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=remove_from_schedule&id='+scheduleId),this.requestOptions);
  }
  updateSchedule(scheduleId,tableId,dateTimeStart,dateTimeEnd,countGuests,reserveComments,guestName,guestPhone) {
  	return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=update_schedule&table_id='+tableId+'&datetime_start='+dateTimeStart+'&datetime_end='+dateTimeEnd+'&id='+scheduleId+'&count_guests='+countGuests+'&comments='+reserveComments+'&guest_name='+guestName+'&guest_phone='+guestPhone),this.requestOptions);
  }
  /* DEPRECATED */
  getReservation(scheduleId) {
  	return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=get_reservation&id='+scheduleId),this.requestOptions);
  }
  addReservation(scheduleId,countGuests,comments) {
  	return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=add_reservation&id='+scheduleId+'&count_guests='+countGuests+'&comments='+comments),this.requestOptions);
  }
  removeReservation(scheduleId) {
  	return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=remove_reservation&id='+scheduleId),this.requestOptions);
  }
  updateReservation(scheduleId,countGuests,comments) {
  	return this._http.get<Response[]>(encodeURI(this.apiUrl+'?action=update_reservation&id='+scheduleId+'&count_guests='+countGuests+'&comments='+comments),this.requestOptions);
  }
}
