import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FormTableComponent } from './form/table/form-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../ngmaterial/ngmaterial.module';
import { PopupTableComponent } from './form/popup/table/popup-table.component';
import { PopupContentTableComponent } from './form/popup-content/table/popup-content-table.component';
import { MatSnackBar, MAT_SNACK_BAR_DATA, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { ApiService, Response } from './services/api.service';
import { FormService } from './form/form.service';
import { RouterModule } from '@angular/router';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { TableDeleteComponent } from './form/popup-content/table/table-delete.component';
import { FormReserveComponent } from './form/reserve/form-reserve.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ReserveComponent } from './form/popup/reserve/reserve.component';
import { ReserveContentComponent } from './form/popup-content/reserve/reserve.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { ReserveRemoveComponent } from './form/popup-content/reserve-remove/reserve-remove.component';


@NgModule({
  declarations: [
    AppComponent,
    FormTableComponent,
    PopupTableComponent,
    PopupContentTableComponent,
    MainNavComponent,
    TableDeleteComponent,
    FormReserveComponent,
    CalendarComponent,
    ReserveComponent,
    ReserveContentComponent,
    ReserveRemoveComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: 'tables', component: FormTableComponent},
      {path: 'calendar', component: CalendarComponent},
      {path: 'reserves', component: FormReserveComponent},
      {path: '', component: FormReserveComponent},
      ]),
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    NgxMaterialTimepickerModule,
  ],
  exports: [
    PopupContentTableComponent,
    ReserveContentComponent,
    ReserveRemoveComponent,
  ],
  entryComponents: [
    PopupContentTableComponent,
    TableDeleteComponent,
    FormReserveComponent,
    CalendarComponent,
    ReserveComponent,
    ReserveContentComponent,
    ReserveRemoveComponent,
  ],
  providers: [
    FormService,
    ApiService,
    ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
  }
}
