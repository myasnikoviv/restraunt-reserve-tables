import { Component, OnInit } from '@angular/core';
import { FormService } from '../form.service'

@Component({
  selector: 'app-form-table',
  templateUrl: './form-table.component.html',
  styleUrls: ['./form-table.component.scss']
})

export class FormTableComponent implements OnInit {

  breakpoint: number;

  constructor(public _FormService: FormService) { 
  }

  ngOnInit() {
  	this._FormService.getTables();
  	this.breakpoint = (window.innerWidth <= 768) ? 1 : 2;
  }

  onResize(event) {
	  this.breakpoint = (event.target.innerWidth <= 768) ? 1 : 2;
	}
  

}
