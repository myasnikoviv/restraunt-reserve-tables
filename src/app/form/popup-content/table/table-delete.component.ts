import { Component, OnInit , Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormService } from '../../form.service'

export interface DialogData {
  name: string;
  number: number;
  max: number;
  comments: string;
}


@Component({
  selector: 'app-table-delete',
  templateUrl: './table-delete.component.html',
  styleUrls: ['./table-delete.component.scss']
})
export class TableDeleteComponent implements OnInit {

  constructor(
	public dialogRef: MatDialogRef<TableDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public _FormService: FormService
  	) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit(result) {
  	this._FormService.removeTable(result.id);
    this._FormService.getTables();
    this._FormService._lastReturnStatus
      .subscribe((value: string) => {
        if (value == 'success') {
          this.dialogRef.close();
        }
      });
  }

  ngOnInit() {
  }

}
