import { Component, OnInit , Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormService } from '../../form.service'

export interface DialogData {
  name: string;
  number: number;
  max: number;
  comments: string;
  coordinate_x: number;
  coordinate_y: number;
  visual_type: number;
}

@Component({
  selector: 'app-popup-content-table',
  templateUrl: './popup-content-table.component.html',
  styleUrls: ['./popup-content-table.component.scss']
})

export class PopupContentTableComponent implements OnInit {

  constructor(
	public dialogRef: MatDialogRef<PopupContentTableComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public _FormService: FormService
  	) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit(result) {
    if (!result.ifadd) {
    this._FormService.updateTable(result.id, result.name, result.max, result.number, result.comments, result.coordinate_x, result.coordinate_y, result.visual_type);
    this._FormService._lastReturnStatus
      .subscribe((value: string) => {
        if (value == 'success') {
          this._FormService.getTables();
          this.dialogRef.close();
        }
      });
    } else {
      if (result.name === undefined) { result.name = '';}
      if (result.max === undefined) { result.max = 2;}
      if (result.number === undefined) { result.number = 0;}
      if (result.comments === undefined) { result.comments = '';}
      if (result.coordinate_x === undefined) { result.coordinate_x = 0;}
      if (result.coordinate_y === undefined) { result.coordinate_y = 0;}
      if (result.visual_type === undefined) { result.visual_type = 1;}
      this._FormService.addTable(result.name, result.max, result.number, result.comments, result.coordinate_x, result.coordinate_y, result.visual_type);
      this._FormService._lastReturnStatus
        .subscribe((value: string) => {
          if (value == 'success') {
            this._FormService.getTables();
            this.dialogRef.close();
          }
        });
    }
  }

  ngOnInit() {
  }

}
