import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupContentTableComponent } from './popup-content-table.component';

describe('PopupContentTableComponent', () => {
  let component: PopupContentTableComponent;
  let fixture: ComponentFixture<PopupContentTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupContentTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupContentTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
