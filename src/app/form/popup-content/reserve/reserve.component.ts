import { Component, OnInit , Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormService } from '../../form.service'
import { FormControl } from '@angular/forms';

export interface DialogData {
  table_id: number;
  id: number;
  date_time_start: string;
  date_time_end: string;
  count_guests: number;
  reserve_comments: string;
  time_start: string;
  time_gap: string;
  guest_name: string;
  guest_phone: string;
}

export interface Interval{
	minutes: number;
	text: string;
}

@Component({
  selector: 'app-reserve-content',
  templateUrl: './reserve.component.html',
  styleUrls: ['./reserve.component.scss']
})

export class ReserveContentComponent implements OnInit {

  public interval: Interval[] = [];
  public curDate;

  setIntervals(){
  	for (var minutes = 30; minutes <= 12*60; minutes += 30) {
  		var txt = Math.trunc(minutes/60) + ':' + (minutes%60 == 0? '00' : minutes%60);
  		this.interval.push({minutes:minutes,text:txt});
  	}
  }

  setCurrentDateString() {
  	this._FormService._curDateFormControl = new FormControl(new Date());
  }


  constructor(public dialogRef: MatDialogRef<ReserveContentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public _FormService: FormService
    ) { }

   onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit(result) {
  	if (this._FormService._curDateFormControl.value === undefined || result.time_start === undefined || result.time_gap === undefined ) {
  		this._FormService.showNotification('failed','Заполните поля даты и времени');
  		return false;
  	}
  	if (result.table_id === undefined) {
  		this._FormService.showNotification('failed','Выберите стол');
  		return false;
  	}
  	
  	var dateTimeStartTimestamp = new Date(this._FormService._curDateFormControl.value);
  	var startYear = dateTimeStartTimestamp.getFullYear();
  	var startMonth = dateTimeStartTimestamp.getMonth() + 1;
  	var startDay = dateTimeStartTimestamp.getDate();
  	var startDayStr = startYear + '-' + (startMonth< 10 ? '0'+startMonth : startMonth) + '-' + (startDay< 10 ? '0'+startDay : startDay) + ' ' + result.time_start + ':00';
    var newStartDayTimestamp = (new Date(startDayStr)).getTime();
  	var endDayTimestamp = newStartDayTimestamp + result.time_gap*60000;
  	var endDate = new Date(endDayTimestamp);
  	var endYear = endDate.getFullYear();
  	var endMonth = endDate.getMonth() + 1;
  	var endDay = endDate.getDate();
  	var endHour = endDate.getHours();
  	var endMinute = endDate.getMinutes();
  	var endDayStr = endYear + '-' + (endMonth< 10 ? '0'+endMonth : endMonth) + '-' + (endDay< 10 ? '0'+endDay : endDay) + ' ' + (endHour< 10 ? '0'+endHour : endHour) + ':' + (endMinute< 10 ? '0'+endMinute : endMinute) + ':00';
      if (!result.ifadd) {
      this._FormService.updateSchedule(result.id,result.table_id,this._FormService.convertToTimestamp(startDayStr),this._FormService.convertToTimestamp(endDayStr),result.count_guests,result.reserve_comments,result.guest_name,result.guest_phone);
      this._FormService._lastReturnStatusSchedule
        .subscribe((value: string) => {
          if (value == 'success') {
            this.dialogRef.close();
          }
        });
      } else {
        if (result.count_guests === undefined) {
          result.count_guests = 1;
        }
        if (result.reserve_comments === undefined) {
          result.reserve_comments = '';
        }
        this._FormService.addToSchedule(result.table_id, this._FormService.convertToTimestamp(startDayStr), this._FormService.convertToTimestamp(endDayStr),result.count_guests,result.reserve_comments,result.guest_name,result.guest_phone);
        this._FormService._lastReturnStatusSchedule
          .subscribe((value: string) => {
            if (value == 'success') {
              this.dialogRef.close();
            }
          });
      }

  }

  ngOnInit() {
  	this.setIntervals();
  }

}
