import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReserveRemoveComponent } from './reserve-remove.component';

describe('ReserveRemoveComponent', () => {
  let component: ReserveRemoveComponent;
  let fixture: ComponentFixture<ReserveRemoveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReserveRemoveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReserveRemoveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
