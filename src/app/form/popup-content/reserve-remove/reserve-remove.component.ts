import { Component, OnInit , Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormService } from '../../form.service'

export interface DialogData {
  table_id: number;
  id: number;
  date_time_start: string;
  date_time_end: string;
  count_guests: number;
  reserve_comments: string;
  time_start: string;
  time_gap: string;
  guest_name: string;
  guest_phone: string;
}

@Component({
  selector: 'app-reserve-remove',
  templateUrl: './reserve-remove.component.html',
  styleUrls: ['./reserve-remove.component.scss']
})

export class ReserveRemoveComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ReserveRemoveComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public _FormService: FormService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit(result) {
  	this._FormService.removeFromSchedule(result.id);
    this._FormService._lastReturnStatusSchedule
      .subscribe((value: string) => {
        if (value == 'success') {
          this.dialogRef.close();
        }
      });
  }

  ngOnInit() {
  }

}
