import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA , MatSnackBar, MAT_SNACK_BAR_DATA } from '@angular/material';
import { ReserveContentComponent } from '../../popup-content/reserve/reserve.component';
import { ReserveRemoveComponent } from '../../popup-content/reserve-remove/reserve-remove.component';
import { ApiService, Response } from '../../../services/api.service';
import { FormService } from '../../form.service'
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-reserve',
  templateUrl: './reserve.component.html',
  styleUrls: ['./reserve.component.scss']
})

export class ReserveComponent implements OnInit {

  @Input() add;
  @Input() ScheduleId;
  @Input() tableId;
  @Input() dateTimeStart;
  @Input() dateTimeEnd;
  @Input() countGuests;
  @Input() reserveComments;
  @Input() guestName;
  @Input() guestPhone;


  constructor(public dialog: MatDialog,
    public _FormService: FormService) { }

  redactReserveDialog(ScheduleId, tableId, dateTimeStart, dateTimeEnd, countGuests, reserveComments, guestName, guestPhone, ifAdd): void {
    this._FormService._curDateFormControl = new FormControl(new Date(dateTimeStart));
    var timeStart = '09:00';
    var timeGap = 60;
    if (!ifAdd) {
      var tmpDate = new Date(dateTimeStart);
      timeStart = (tmpDate.getHours()<10 ? '0' + tmpDate.getHours() : tmpDate.getHours()).toString() + ':' + (tmpDate.getMinutes()<10 ? '0' + tmpDate.getMinutes() : tmpDate.getMinutes()).toString();
      timeGap = ( (new Date(dateTimeEnd)).getTime() - (new Date(dateTimeStart)).getTime()  )/60000;
    }
    const dialogRef = this.dialog.open(ReserveContentComponent, {
      width: '600px',
      data: {id: this.ScheduleId, table_id: this.tableId, date_time_start: this.dateTimeStart, time_start: timeStart, time_gap: timeGap, date_time_end: this.dateTimeEnd, count_guests: this.countGuests, reserve_comments: this.reserveComments, guest_name: this.guestName, guest_phone: this.guestPhone, ifadd: ifAdd}
    });
  }

  deleteReserveDialog(ScheduleId, tableId, dateTimeStart, dateTimeEnd, countGuests, reserveComments, guestName, guestPhone, ifAdd) {
    this._FormService._curDateFormControl = new FormControl(new Date(dateTimeStart));
    var timeStart = '09:00';
    var timeGap = 60;
    if (!ifAdd) {
      var tmpDate = new Date(dateTimeStart);
      timeStart = (tmpDate.getHours()<10 ? '0' + tmpDate.getHours() : tmpDate.getHours()).toString() + ':' + (tmpDate.getMinutes()<10 ? '0' + tmpDate.getMinutes() : tmpDate.getMinutes()).toString();
      timeGap = ( (new Date(dateTimeEnd)).getTime() - (new Date(dateTimeStart)).getTime()  )/60000;
    }
    const dialogRef = this.dialog.open(ReserveRemoveComponent, {
      width: '600px',
      data: {id: this.ScheduleId, table_id: this.tableId, date_time_start: this.dateTimeStart, time_start: timeStart, time_gap: timeGap, date_time_end: this.dateTimeEnd, count_guests: this.countGuests, reserve_comments: this.reserveComments, guest_name: this.guestName, guest_phone: this.guestPhone, ifadd: ifAdd}
    });
  }

  ngOnInit() {
  }

}
