import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA , MatSnackBar, MAT_SNACK_BAR_DATA } from '@angular/material';
import { PopupContentTableComponent } from '../../popup-content/table/popup-content-table.component';
import { TableDeleteComponent } from '../../popup-content/table/table-delete.component';
import { ApiService, Response } from '../../../services/api.service';
import { FormService } from '../../form.service'

@Component({
  selector: 'app-popup-table',
  templateUrl: './popup-table.component.html',
  styleUrls: ['./popup-table.component.scss'],
  providers: []
})

export class PopupTableComponent implements OnInit {

  @Input() add;
  @Input() tableId; 
  @Input() tableName;
  @Input() tableNumber;
  @Input() tableMax;
  @Input() tableAdminComments;
  @Input() coordinateX;
  @Input() coordinateY;
  @Input() visualType;


  constructor(
    public dialog: MatDialog,
    public _FormService: FormService) { }

  redactTableDialog(tableId, tableName, tableNumber, tableMax, tableAdminComments, ifAdd, coordinateX, coordinateY, visualType): void {
    
    const dialogRef = this.dialog.open(PopupContentTableComponent, {
      width: '600px',
      data: {id: this.tableId, name: this.tableName, number: this.tableNumber, max: this.tableMax, comments: this.tableAdminComments, ifadd: ifAdd, coordinate_x: this.coordinateX, coordinate_y: this.coordinateY, visual_type: this.visualType}
    });
  }
  deleteTableDialog(tableId, tableName, tableNumber, tableMax, tableAdminComments): void {
    const dialogRef = this.dialog.open(TableDeleteComponent, {
      width: '600px',
      data: {id: this.tableId, name: this.tableName, number: this.tableNumber, max: this.tableMax, comments: this.tableAdminComments}
    });
  }
  refreshTables() {
    this._FormService.getTables();
  }

  ngOnInit() {
  }

}
