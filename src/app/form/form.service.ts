import { Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ApiService, Response } from '../services/api.service';
import { map, filter, scan } from '../../../node_modules/rxjs/operators';
import { MatSnackBar, MAT_SNACK_BAR_DATA } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { FormControl } from '@angular/forms';

export interface Table {
  id: number;
  name: string; 
  max: number; 
  table_number: number; 
  admin_comments: string;
  coordinate_x: number;
  coordinate_y: number;
  visual_type: number;
}

export interface Schedule {
  id: number;
  date_time_start: string;
  date_time_end: string;
  table_id: number;
  count_guests: number;
  reserve_comments: string;
  guest_name: string;
  guest_phone: string;
}

export interface DayOfWeek {
  number: number;
  name: string;
  status: string;
}

export interface Month {
  day_number: number;
  day_of_week: DayOfWeek;
  month: number;
  reserves: Array<number>;
}

@Injectable({
  providedIn: 'root'
})

export class FormService {

  private response: Response[];
  public _allTables : Table[] = [];
  public _allReserves: Schedule[] = [];
  public _allMonth: Month[] = [];
  public _lastReturnStatus = new BehaviorSubject(null);
  public _lastReturnStatusSchedule = new BehaviorSubject(null);
  public _tableSelectStatus = new BehaviorSubject(null);
  public _scheduleSelectStatus = new BehaviorSubject(null);
  public _curDateFormControl = new FormControl(new Date());

  constructor(private _apiService : ApiService, private snackBar: MatSnackBar) { }

  set allTables (tableArr: Table[]) {
    this._allTables = tableArr;
  }
  set lastReturnStatus (status : string) {
    this._lastReturnStatus.next(status);
  }
  set lastReturnStatusSchedule (status : string) {
    this._lastReturnStatusSchedule.next(status);
  }
  set tableSelectStatus (status : string) {
    this._tableSelectStatus.next(status);
  }
  set scheduleSelectStatus (status : string) {
    this._scheduleSelectStatus.next(status);
  }
  set allReserves (reserveArr: Schedule[]) {
    this._allReserves = reserveArr;
  }
  set allMonth (month: Month[]) {
    this._allMonth = month;
  }

  convertToTimestamp(dateTimeStr) {
    let date = new Date(dateTimeStr);
    return date.getTime() / 1000;
  }

  getWeekDay(date) {
    var days = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];
    return days[date.getDay()];
  }

  getScheduleItem(scheduleId) {
    return this._allReserves.find(schedule => schedule.id == scheduleId);
  }

  showNotification(type = 'success',message = '') {
    if (message != '') {
      if (type == 'success') {
         this.snackBar.open(message, '', {
          duration: 2000,
          panelClass: ['green-snackbar']
        });
      } else if (type == 'failed') {
        this.snackBar.open(message, '', {
          duration: 2000,
          panelClass: ['red-snackbar']
        });
      }
    }
  }

  getTables() {
    this.allTables = [];
    this._apiService.getTables()
    .subscribe(
      result => {
          this.response = result;
          this.showNotification(this.response['status'],this.response['message']);
          if (this.response['status'] == 'success') {
            var tmpTable: Table[] = [];
            this.response['data'].forEach(function(entry) {
            var mytable = {id: entry.ID, name: entry.NAME, max: entry.MAX_GUESTS, table_number: entry.NUMBER, admin_comments: entry.ADMIN_COMMENTS, coordinate_x: entry.COORDINATE_X, coordinate_y: entry.COORDINATE_Y, visual_type: entry.VISUAL_TYPE};
            tmpTable.push(mytable);
        });
            this.allTables = tmpTable;
        //console.log(this.allTables.find(table => table.id == 10).name);
          }
          this.tableSelectStatus = this.response['status'];
        }
      );
  }

  updateTable(tableId,tableName,tableMaxGuests,tableNumber,tableAdminComments,coordinateX,coordinateY,visualType) {
    this._apiService.updateTable(tableId,tableName,tableMaxGuests,tableNumber,tableAdminComments,coordinateX,coordinateY,visualType)
    .subscribe(
      result => {
        this.response = result;
          this.showNotification(this.response['status'],this.response['message']);
          this.lastReturnStatus = this.response['status'];
        }
      );
  }
  removeTable(tableId) {
    this._apiService.removeTable(tableId)
    .subscribe(
      result => {
        this.response = result;
          this.showNotification(this.response['status'],this.response['message']);
          this.lastReturnStatus = this.response['status'];
        }
      );
  }
  addTable(tableName,tableMaxGuests,tableNumber,tableAdminComments,coordinateX,coordinateY,visualType) {
    this._apiService.addTable(tableName,tableMaxGuests,tableNumber,tableAdminComments,coordinateX,coordinateY,visualType)
    .subscribe(
      result => {
        this.response = result;
          this.showNotification(this.response['status'],this.response['message']);
          this.lastReturnStatus = this.response['status'];
        }
      );
  }
  getSchedule(dateTimeStart,dateTimeEnd) {
    this.allReserves = [];
    this._apiService.getSchedule(this.convertToTimestamp(dateTimeStart),this.convertToTimestamp(dateTimeEnd))
    .subscribe(
      result => {
        this.response = result;
          this.showNotification(this.response['status'],this.response['message']);
          if (this.response['status'] == 'success') {
            var tmpReserve: Schedule[] = [];
            this.response['data'].forEach(function(entry) {
            var myreserve = {id: entry.ID, date_time_start: entry.TIME_START, date_time_end: entry.TIME_END, table_id: entry.TABLE_ID, count_guests: entry.COUNT_GUESTS, reserve_comments: entry.COMMENT, guest_name: entry.GUEST_NAME, guest_phone: entry.GUEST_PHONE};
            tmpReserve.push(myreserve);
        });
            this.allReserves = tmpReserve;
            //console.log(this.allTables.find(table => table.id == 10).name);
          }
          this.scheduleSelectStatus = this.response['status'];
        }
      );
  }
  getCalendar(currentYear: number, currentMonth: number) {
    this.allReserves = [];
    this.allMonth = [];
    var _this = this;
    var tmpDate = new Date(currentYear, currentMonth + 1, 0);
    var lastDayOfMonth = tmpDate.getDate();
    var currentMonthPhp = currentMonth + 1;
    var dateTimeStart = currentYear + '-' + currentMonthPhp + '-01 00:00:00';
    var dateTimeEnd = currentYear + '-' + currentMonthPhp + '-' + lastDayOfMonth +' 23:59:59';
    var tmpMonth: Month[] = [];
    for (var dayNumber = 1; dayNumber <= lastDayOfMonth; dayNumber++) {
      var tmpDate = new Date(currentYear,currentMonth,dayNumber);
      var countDayOfWeek = tmpDate.getDay();//5
      var nameDayOfWeek = this.getWeekDay(tmpDate);//fr
      if (dayNumber == 1 && countDayOfWeek != 1) {
        for (var i = 1; i < countDayOfWeek; i++) {
          var tmpDate = new Date(currentYear,currentMonth,i - countDayOfWeek + 1);//2019 3 1-5
          var thisDayOfMonth = tmpDate.getDate();
          var thisDayOfMonthName = this.getWeekDay(tmpDate);
          var dayOfMonth = {day_number: thisDayOfMonth, day_of_week: {name: thisDayOfMonthName, number: i, status: 'unavailable'}, reserves: [], month: currentMonth - 1};
          tmpMonth.push(dayOfMonth);
        }
      }
      var dayOfMonth = {day_number: dayNumber, day_of_week: {name: nameDayOfWeek, number: countDayOfWeek, status: 'free'}, reserves: [], month: currentMonth};
      tmpMonth.push(dayOfMonth);
    }
    this.allMonth = tmpMonth;
    this._apiService.getSchedule(this.convertToTimestamp(dateTimeStart),this.convertToTimestamp(dateTimeEnd))
    .subscribe(
      result => {
        this.response = result;
          this.showNotification(this.response['status'],this.response['message']);
          if (this.response['status'] == 'success') {
            var tmpReserve: Schedule[] = [];
            this.response['data'].forEach(function(entry) {
            var myreserve = {id: entry.ID, date_time_start: entry.TIME_START, date_time_end: entry.TIME_END, table_id: entry.TABLE_ID, count_guests: entry.COUNT_GUESTS, reserve_comments: entry.COMMENT, guest_name: entry.GUEST_NAME, guest_phone: entry.GUEST_PHONE};
            tmpReserve.push(myreserve);
            var tmpDate = new Date(myreserve.date_time_start);
            var dayStart = tmpDate.getDate();
            var tmpDate = new Date(myreserve.date_time_end);
            var dayEnd = tmpDate.getDate();
            if (dayStart >= 1 && dayEnd <= 32) {
              for (var dayNumber = dayStart; dayNumber <= dayEnd; dayNumber++) {
                _this._allMonth.find(month => month.day_number == dayNumber && month.month == currentMonth).day_of_week.status = 'occupated';
                _this._allMonth.find(month => month.day_number == dayNumber && month.month == currentMonth).reserves.push(myreserve.id);
              }
            }
        });
            this.allReserves = tmpReserve;
        //console.log(this.allTables.find(table => table.id == 10).name);
          }
        }
      );
  }
  updateSchedule(scheduleId,tableId,dateTimeStart,dateTimeEnd,countGuests,reserveComments,guestName,guestPhone) {
    this._apiService.updateSchedule(scheduleId,tableId,dateTimeStart,dateTimeEnd,countGuests,reserveComments,guestName,guestPhone)
    .subscribe(
      result => {
        this.response = result;
          this.showNotification(this.response['status'],this.response['message']);
          this.lastReturnStatusSchedule = this.response['status'];
        }
      );
  }
  addToSchedule(tableId,dateTimeStart,dateTimeEnd,countGuests,reserveComments,guestName,guestPhone) {
    this._apiService.addToSchedule(tableId,dateTimeStart,dateTimeEnd,countGuests,reserveComments,guestName,guestPhone)
    .subscribe(
      result => {
        this.response = result;
          this.showNotification(this.response['status'],this.response['message']);
          this.lastReturnStatusSchedule = this.response['status'];
        }
      );
  }
  removeFromSchedule(scheduleId) {
    this._apiService.removeFromSchedule(scheduleId)
    .subscribe(
      result => {
        this.response = result;
          this.showNotification(this.response['status'],this.response['message']);
          this.lastReturnStatusSchedule = this.response['status'];
        }
      );
  }
  onInit() {
    this.lastReturnStatusSchedule = 'failed';
    this.scheduleSelectStatus = 'failed';
  }
}
