import { Component, OnInit } from '@angular/core';
import { FormService } from '../../form/form.service'

export interface ScheduleCoordinates {
  id: number;
  date_time_start: string;
  date_time_end: string;
  table_id: number;
  top_coordinate: number;
  height: number;
  count_guests: number;
  reserve_comments: string;
  guest_name: string;
  guest_phone: string;
}

export interface ScheduleByTable {
	table_id: number;
	reserves: Array<ScheduleCoordinates>;
}

export interface TimeLine {
	height: number;
	top: number;
	time: string;
}

@Component({
  selector: 'app-form-reserve',
  templateUrl: './form-reserve.component.html',
  styleUrls: ['./form-reserve.component.scss']
})
export class FormReserveComponent implements OnInit {

  public _scheduleByTable: ScheduleByTable[] = [];
  public dateTimeStart: number;
  public dateTimeEnd: number;
  public timeGap: number;
  public timeLine: TimeLine[] = [];
  public day: string;
  public timeStart: string;
  public timeEnd: string;

  constructor(public _FormService: FormService) { }

  createDaySchedule() {
  	var _this = this;
  	_this._FormService._tableSelectStatus
      .subscribe((value: string) => {
        if (value == 'success') {
        	_this._FormService._scheduleSelectStatus
		      .subscribe((value: string) => {
		        if (value == 'success') {
		        	_this._scheduleByTable = [];
		        	_this._FormService._allTables.forEach(function(table){
				  		var reservesByDay = _this._FormService._allReserves.filter(reserve => reserve.table_id == table.id);
				  		var reservesByDayCoordinates:ScheduleCoordinates[] = [];
				  		if (reservesByDay !== undefined && reservesByDay.length != 0) {
				  			reservesByDay.forEach(function(reserveByCurrentTable,key){
				  				var topCoordinatePercent = -((_this.dateTimeStart - _this.checkDateInbound(reserveByCurrentTable.date_time_start))*100)/_this.timeGap;
				  				topCoordinatePercent = parseFloat(topCoordinatePercent.toFixed(2));
				  				var heightPercent = ((_this.checkDateInbound(reserveByCurrentTable.date_time_end) - _this.checkDateInbound(reserveByCurrentTable.date_time_start))*100)/_this.timeGap;
				  				heightPercent = parseFloat(heightPercent.toFixed(2));
				  				reservesByDayCoordinates[key] =
				  				{
				  					id:reserveByCurrentTable.id,
				  					date_time_start:reserveByCurrentTable.date_time_start,
				  					date_time_end:reserveByCurrentTable.date_time_end,
				  					table_id:reserveByCurrentTable.table_id,
				  					top_coordinate:topCoordinatePercent,
				  					height:heightPercent,
                    count_guests: reserveByCurrentTable.count_guests,
                    reserve_comments: reserveByCurrentTable.reserve_comments,
                    guest_name: reserveByCurrentTable.guest_name,
                    guest_phone: reserveByCurrentTable.guest_phone
                  };
				  			});
				  			_this._scheduleByTable.push({table_id:table.id,reserves: reservesByDayCoordinates});
				  		}
				  		
				  	});
		        }
		      });
        }
      });
  }

  convertToJsTimestamp(dateTimeStr) {
    let date = new Date(dateTimeStr);
    return date.getTime();
  }

  checkDateInbound(dateTimeStr) {
  	let date = new Date(dateTimeStr);
  	if (date.getTime() < this.dateTimeStart) {
  		return this.dateTimeStart;
  	} else if (date.getTime() > this.dateTimeEnd) {
  		return this.dateTimeEnd;
  	} else {
  		return date.getTime();
  	}
  }

   getMonthName(monthNumber) {
  	var monthes = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
  	return monthes[monthNumber];
  }

  setTimes(dateTimeStart,dateTimeEnd){
    this.dateTimeStart = this.convertToJsTimestamp(dateTimeStart);
    this.dateTimeEnd = this.convertToJsTimestamp(dateTimeEnd);
    this.timeGap = this.dateTimeEnd - this.dateTimeStart;
	var tmpDate = new Date(this.dateTimeStart);
    this.day = tmpDate.getDate() + ' ' + this.getMonthName(tmpDate.getMonth());
  }

  getTableById(tableId) {
  	return this._FormService._allTables.find(table => table.id == tableId);
  }

  setTimeLine(minutes) {
  	var milliseconds = minutes * 60000;
  	var height = milliseconds * 100 / this.timeGap;
  	height = parseFloat(height.toFixed(2));
  	var countOfLines = this.timeGap / milliseconds;
  	for (var line = 0; line <= countOfLines; line++) {
  		var curTimeLine = this.dateTimeStart + line * milliseconds;
  		var tmpDate = new Date(curTimeLine);
  		var timeStr = tmpDate.getHours() + ':' + (tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
  		var curGap = line * height;
  		curGap = parseFloat(curGap.toFixed(2));
  		this.timeLine.push({height:height,top:curGap,time:timeStr});
  	}
  }

  getTimeFromDateTime(dateTime) {
  	var tmpDate = new Date(dateTime);
  	var timeStr = tmpDate.getHours() + ':' + (tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
  	return timeStr;
  }

  getTimeGap(dateTimeStart,dateTimeEnd) {
  	var tmpDate = new Date(dateTimeStart);
  	var startMs = tmpDate.getTime();
  	var tmpDate = new Date(dateTimeEnd);
  	var endMs = tmpDate.getTime();
  	return (endMs - startMs)/3600000;
  }

  hourWordForm(hour) {
  	if (hour < 1) {
  		return 'минут';
  	} else if (hour == 1) {
  		return 'час';
  	} else if (hour > 1) {
  		return 'часа';
  	}
  }

  prevDay() {
	var tmpDate = new Date(this.dateTimeStart);
	var curDay = tmpDate.getDate();
  	var prevDate = new Date(tmpDate.getFullYear(), tmpDate.getMonth(), curDay - 1);
  	var strMonth = prevDate.getMonth() + 1;
  	var Ymd = prevDate.getFullYear() + '-' + strMonth + '-' + prevDate.getDate();
  	this.refreshDaySchedule(Ymd,Ymd);
  }
  nextDay() {
  	var tmpDate = new Date(this.dateTimeStart);
  	var curDay = tmpDate.getDate();
  	var nextDate = new Date(tmpDate.getFullYear(), tmpDate.getMonth(), curDay + 1);
  	var strMonth = nextDate.getMonth() + 1;
  	var Ymd = nextDate.getFullYear() + '-' + strMonth + '-' + nextDate.getDate();
  	this.refreshDaySchedule(Ymd,Ymd);
  }

  refreshDaySchedule(YmdStart, YmdEnd) {
  	this.setTimes(YmdStart + ' ' + this.timeStart,YmdEnd + ' ' + this.timeEnd);
  	this._FormService.getSchedule(this.dateTimeStart,this.dateTimeEnd);
  	this.createDaySchedule();
  }
  refreshCurrentDay() {
  	this._FormService.getSchedule(this.dateTimeStart,this.dateTimeEnd);
  	this.createDaySchedule();
  }

  ngOnInit() {
  	this.timeStart = '11:00:00';
  	this.timeEnd = '23:59:59';
    var curDate = new Date();
    var curMonth = ((curDate.getMonth() + 1) < 10) ? '0' + (curDate.getMonth() + 1) : (curDate.getMonth() + 1);
    var curDay = (curDate.getDate() < 10) ? '0' + curDate.getDate() : curDate.getDate();
    var curDateStr = curDate.getFullYear() + '-' + curMonth + '-' + curDay;
  	this.setTimes( curDateStr + ' ' + this.timeStart, curDateStr + ' ' + this.timeEnd);
  	this._FormService.getSchedule(this.dateTimeStart,this.dateTimeEnd);
  	this._FormService.getTables();
  	this.createDaySchedule();
  	this.setTimeLine(30);
    this._FormService._lastReturnStatusSchedule
      .subscribe((value: string) => {
        if (value == 'success') {
          this.refreshCurrentDay();
        }
      });
  }

}
