import { Component, OnInit } from '@angular/core';
import { FormService } from '../form/form.service'

export interface CurrentMonth {
  name: string;
  number: number;
}

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  
  public month: CurrentMonth;
  public year: number;

  constructor(public _FormService: FormService) { 
  	var curDate = new Date();
  	this.month = this.getMonthObj(curDate);
  	this.year = curDate.getFullYear();
  }

  getMonthObj(date) {
  	var number = date.getMonth();
  	var name = this.getMonthName(number);
  	return {name: name,number: number};
  }

  getMonthName(monthNumber) {
  	var monthes = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
  	return monthes[monthNumber];
  }

  nextMonth() {
  	if (this.month.number == 11) {
  		this.year++;
  		var curDate = new Date(this.year,0,1);
  		this.month = this.getMonthObj(curDate);
  	} else {
  		var curDate = new Date(this.year,this.month.number+1,1);
  		this.month = this.getMonthObj(curDate);
  	}
  	this._FormService.getCalendar(this.year,this.month.number);
  }

  prevMonth() {
  	if (this.month.number == 0) {
  		this.year--;
  		var curDate = new Date(this.year,11,1);
  		this.month = this.getMonthObj(curDate);
  	} else {
  		var curDate = new Date(this.year,this.month.number-1,1);
  		this.month = this.getMonthObj(curDate);
  	}
  	this._FormService.getCalendar(this.year,this.month.number);
  }

  ngOnInit() {
  	this._FormService.getCalendar(this.year,this.month.number);
  }

}
